Schleuder Gitlab Ticketing
==========================

A schleuder filter/plugin to hook lists into the issue tracker of a gitlab project.

Background
----------

Schleuderlists are not only a helpful tool for communication within groups, they can also be
used for newsletters, or by using its famous remailer capabilities to be used as a contact
address for a project or as a help desk.

In the latter two cases keeping an overview of the different interactions can become somewhat
cumbersome, especially in a bigger collective where not everybody is able to dedicate the same
amount of time/attention to the contact address / help desk. Hence it is easy that a certain
thread (and so task) on the list is getting lost. Having some kind of ticketing system in place
can help to easily keep track of what is done, what is in progress and what should be looked at.

Additionally, folks do not like to double the work and hence as much as possible should be done
while emails flow over the list.

This schleuder filter / plugin allows to map emails through an id in the subject to issues in
a gitlab project. Additionally, it opens an issue on new emails or closes issues if the subject
indicates that the job is done.

The gitlab issues are mainly used to keep the state of a certain email thread and contains only
as much plaintext information as was transmitted in plaintext over the wire. This means it only
stores From:, Subject: and Message-Id: as issue comments.

How it works
------------

The filter / plugin hooks into the filter process list in schleuder. First schleuder will check
whether there is a configuration for the current list, as well as whether this configuration
is correct. Otherwise the filter is immediately skipped.

Schleuder then dispatches the processing to the specific list instance, which checks whether
the email comes from a sender that shall be ignored (e.g. well known spammers or announce lists)
or whether the subject matches any of the configured subject filters. The email is also ignored
if the well known `X-Spam-Flag` header is set to `yes`.

If none of that matches, the email is being processed and first the email's subject is analyzed
to get an already existing ticket id. The plugin expects something like `Subject: my subject [gp#123]`,
where gp is a configureable project identifier and 123 matches an issue id in gitlab. The brackets
enclose the ticket identifier and can be anywhere within the subject.

If no ticket id can be found or no issue can be found in gitlab a new issue will be created in
gitlab. The ticket id will be appended to the subject, a faulty ticket id will be replaced.

If a ticket can be found, a comment is added to the issue, that includes the sender email address,
as well as the Subject and Message-ID of the email.

Additionally, if the email is sent from an email address, that is a member of the gitlab project
the ticket will be assigned to said user. Furthermore, the label `inprocess` is added to ticket
to indicate that someone of the project is working on that thread.

If the ticket is already closed it will be re-opened.

If the subject contains: `[ok]` the ticket will be closed and the label `inprocess` removed. An
already closed ticket stays closed.

That's it.

Prerequisits
------------

* ruby >=2.1
* schleuder >= 3.3.0
* A working schleuder installation and a schleuder list
* A gitlab project and a dedicated user and its API token

Installation
------------

* Install the gem (depends on the gitlab gem)
* Link `lib/schleuder/filters/post_decrpytion/99_gitlab_ticketing.rb` into `/usr/local/lib/schleuder/filters/post_decryption/`,
  so the plugin gets loaded.

Configuration
-------------

The plugin is looking for a configuration in `/etc/schleuder/gitlab.yml`. The configuration file
expects a `gitlab` and a lists `configuration` section:

```
gitlab:
  endpoint: https://gitlab.example.com/api/v4
  token: xxxx

lists:
  test@schleuder.example.com:
    project: tickets
    namespace: support
```

The key of the lists configuration indicates the listname on schleuder side, which matches its
email address. A list's setting requires 2 configuration options: `project` & `namespace` which
map to the path within gitlab, where the project exists. Namespace can either be the group or
the users name, depending of where a project lives.

The plugin supports working for multiple lists, as well as allows individual gitlab configuration
for different lists:

```
gitlab:
  endpoint: https://gitlab.example.com/api/v4
  token: xxxx
lists:
  test@schleuder.example.com:
    project: tickets
    namespace: support
  test2@schleuder.example.com:
    gitlab:
      endpoint: https://gitlab2.example.com/api/v4
      token: aaaa
```

Additionally, you can specify filters based on the sender or the subject. They must be valid ruby
regexps and can also be specified on a global and local level.

```
gitlab:
  endpoint: https://gitlab.example.com/api/v4
  token: xxxx

subject_filters:
  - '\[announce\]'
sender_filters:
  - '^spammer@example\.com$'

lists:
  test@schleuder.example.com:
    project: tickets
    namespace: support
    subject_filters:
      - 'ignore me'
  test2@schleuder.example.com:
    gitlab:
      endpoint: https://gitlab2.example.com/api/v4
      token: aaaa
    sender_filters:
      - 'noreply@example\.com'

```

Additionally, you can specify a specifc identifier to more easily identify a particular ticket id in
the subject:

```
lists:
  test@schleuder.example.com:
    project: tickets
    namespace: support
    ticket_prefix: ourid
```

This will look for the following string in a subject: `[ourid#\d+]`. By default it would look for:
`[gl/test#\d+]`

Todo
----

This plugin is in a working state and has been tested by multiple use cases for nearly a year.

Some more ideas:

* Use schleuder's verification capabilities to further prove that the email comes from a list member.
* Hook into schleuder's plugin capabilities, to allow more ticket actions (e.g. assign, labels, ...)

Contributing
------------

Please see [CONTRIBUTING.md](CONTRIBUTING.md).


Mission statement
-----------------

Please see [MISSION_STATEMENT.md](MISSION_STATEMENT.md).


Code of Conduct
---------------

We adopted a code of conduct. Please read [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md).


License
-------

GNU GPL 3.0. Please see [LICENSE.txt](LICENSE.txt).
