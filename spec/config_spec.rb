require 'spec_helper'
describe SchleuderGitlabTicketing::Config do

  describe 'non existing config' do
    let(:config){ SchleuderGitlabTicketing::Config.new('spec/fixtures/gitlab.yml.nonexistent') }
    context 'with a config' do
      it 'provides a way to process a list' do
        expect(config).to respond_to(:process_list)
      end
      it 'loads the configured lists' do
        expect(config.lists.length).to eql(0)
      end
    end
  end

  describe 'basic' do
    let(:config){ SchleuderGitlabTicketing::Config.new('spec/fixtures/gitlab.yml') }
    context 'with a config' do
      it 'provides a way to process a list' do
        expect(config).to respond_to(:process_list)
      end
      it 'loads the configured lists' do
        expect(config.lists.length).to eql(8)
        expect(config.lists.values.first).to be_instance_of(SchleuderGitlabTicketing::List)
      end
    end
    context 'gitlab config' do
      it 'adds the global gitlab config to all the lists' do
        test_list = config.lists['test']
        expect(test_list.gitlab.endpoint).to eql('https://gitlab.example.com/api/v4')
        expect(config.lists['invalid1'].gitlab).to eql(test_list.gitlab)
      end
      it 'supports dedicated gitlabs for certain lists' do
        test_list = config.lists['test2']
        expect(test_list.gitlab.endpoint).to eql('https://gitlab2.example.com/api/v4')
        expect(config.lists['invalid1'].gitlab).to_not eql(test_list.gitlab)
      end
    end
    context 'merging filters' do
      it 'merges global and local subject filters' do
        test_list = config.lists['test']
        expect(test_list.subject_filters.length).to eql(2)
        expect(test_list.subject_filters).to include('\[announce\]')
        expect(test_list.subject_filters).to include('ignore me')
        test_list = config.lists['test2']
        expect(test_list.subject_filters.length).to eql(1)
        expect(test_list.subject_filters).to include('\[announce\]')
        expect(test_list.subject_filters).to_not include('ignore me')
      end
      it 'merges global and local sender filters' do
        test_list = config.lists['test2']
        expect(test_list.sender_filters.length).to eql(2)
        expect(test_list.sender_filters).to include('^test@example\.com$')
        expect(test_list.sender_filters).to include('noreply@example\.com')
        test_list = config.lists['test']
        expect(test_list.sender_filters.length).to eql(1)
        expect(test_list.sender_filters).to include('^test@example\.com$')
        expect(test_list.sender_filters).to_not include('noreply@example\.com')
      end
    end
  end
end

