Change Log
==========

This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] / 2019-02-21

* initial release
