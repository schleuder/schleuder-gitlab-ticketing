module SchleuderGitlabTicketing
  module GitlabConfig
    def gitlab
      @gitlab ||= if @config['gitlab']['endpoint'] && @config['gitlab']['token']
        Gitlab.client(endpoint: @config['gitlab']['endpoint'], private_token: @config['gitlab']['token'])
      else
        nil
      end
    end
  end
end
