module Schleuder
  module Filters
    def self.gitlab_ticketing(list, mail)
      @gt_config ||= begin
        require 'schleuder-gitlab-ticketing'
        SchleuderGitlabTicketing::Config.new
      end

      res = @gt_config.process_list(list.email, mail)
      if res.nil?
        list.logger.debug('No gitlab ticketing enabled for list')
      elsif res == 'config-error'
        list.logger.error('gitlab ticketing not correctly configured')
      elsif res == false
        list.logger.debug('Email skipped by list configuration')
      end
      nil
    # make sure we catch any error with that filter
    # so we don't affect list processing
    rescue Exception => e
      list.logger.notify_admin "Unable to process the following mail with gitlab-ticketing:\n\n#{e}\n\n#{e.backtrace.join("\n")}", mail.original_message, 'Error while processing mail with gitlab-ticketing'
      nil
    end
  end
end
